# config-files.md

> Note - See README.md in project-root

The Vagrantfile and config.yml files present should be copied into the drupal-vm/ directory 'as is', with no alterations.

Any changes to the Vagrantfile or config.yml files should be made here, then updated on the copy within the drupal-vm/ folder.

Command:
```
cp ~/utexas-drupalvm-projects/redesign/config-files/config.yml ~/utexas-drupalvm-projects/drupal-vm/config.yml
cp ~/utexas-drupalvm-projects/redesign/config-files/Vagrantfile ~/utexas-drupalvm-projects/drupal-vm/Vagrantfile
```
